vpc = {
  cidr                 = "10.100.0.0/18"
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true
}

eks = {
  cluster_version = "1.21"
  spot = {
    min_size      = 3
    max_size      = 5
    desired_size  = 3
    instance_type = "t3.medium"
  }
}

region = "ap-southeast-1"

private_subnets = ["10.100.0.0/22", "10.100.4.0/22", "10.100.8.0/22"]

public_subnets = ["10.100.12.0/23", "10.100.14.0/23", "10.100.16.0/23"]

environment = "dev"

agent_token = "hL41vLQ-8Sjvcwqgo2s7D8ycHWniiYvJ5tn_jNkACYu-_KrbGA"
