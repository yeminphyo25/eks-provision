locals {
  environment     = var.environment
  name            = "eko-${var.environment}"
  cluster_version = var.eks.cluster_version
  region          = var.region
  common_tags = {
    Environment = local.environment
    Terraform   = "true"
  }
}

##################################################
# Data Field
##################################################

data "aws_availability_zones" "az_list" {
  filter {
    name   = "opt-in-status"
    values = ["opt-in-not-required"]
  }
}

data "aws_eks_cluster_auth" "this" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

##################################################

resource "aws_kms_key" "cluster_encrypt" {
  description             = "KMS Key to encrypt eks secrets"
  deletion_window_in_days = 7
  enable_key_rotation     = true
  tags                    = local.common_tags
}

resource "aws_security_group" "additional" {
  name_prefix = "${local.name}-additional"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    cidr_blocks = [
      "10.0.0.0/8",
      "172.16.0.0/12",
      "192.168.0.0/16",
    ]
  }
  tags = local.common_tags
}

#resource "aws_iam_policy" "policy" {
#  name        = "worker-policy"
#  description = "Additional Workers Policy"
#  policy      = file("files/iam-alb.json")
#}

#resource "aws_iam_role_policy_attachment" "additional" {
#  for_each   = module.eks.self_managed_node_groups
#  policy_arn = aws_iam_policy.policy.arn
#  role       = each.value.iam_role_name
#}

##################################################
# EKS Module
##################################################

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "18.10.0"

  cluster_name                    = "${local.name}-eks"
  cluster_version                 = local.cluster_version
  cluster_endpoint_private_access = true
  cluster_endpoint_public_access  = true

  cluster_addons = {
    coredns = {
      resolve_conflicts = "OVERWRITE"
    }
    vpc-cni = {
      resolve_conflicts = "OVERWRITE"
    }
    kube-proxy = {}
  }

  cluster_encryption_config = [{
    provider_key_arn = aws_kms_key.cluster_encrypt.arn
    resources        = ["secrets"]
  }]

  subnet_ids = module.vpc.private_subnets
  vpc_id     = module.vpc.vpc_id

  cluster_security_group_additional_rules = {
    egress_nodes_ephemeral_ports_tcp = {
      description                = "To node 1025-655335"
      protocol                   = "tcp"
      from_port                  = 1025
      to_port                    = 65535
      type                       = "egress"
      source_node_security_group = true
    }
  }

  node_security_group_additional_rules = {
    ingress_self_all = {
      description = "Node to node all ports/protocols"
      protocol    = "-1"
      from_port   = 0
      to_port     = 0
      type        = "ingress"
      self        = true
    }
    egress_all = {
      description      = "Node all egress"
      protocol         = "-1"
      from_port        = 0
      to_port          = 0
      type             = "egress"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
  }

  self_managed_node_group_defaults = {
    iam_role_additional_policies = ["arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"]
    vpc_security_group_ids       = [aws_security_group.additional.id]
    instance_type                = var.eks.spot.instance_type
    enable_monitoring            = true
  }

  self_managed_node_groups = {
    spot_ng = {
      name         = "eks-spot-nodegroup"
      min_size     = var.eks.spot.min_size
      max_size     = var.eks.spot.max_size
      desired_size = var.eks.spot.desired_size
      instance_market_options = {
        market_type = "spot"
      }

      bootstrap_extra_args     = "--kubelet-extra-args '--node-labels=node.kubernetes.io/lifecycle=spot'"
      post_bootstrap_user_data = <<-EOT
        cd /tmp
        sudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
        sudo systemctl enable amazon-ssm-agent
        sudo systemctl start amazon-ssm-agent
        EOT
    }
  }

  tags = local.common_tags
}

locals {
  kubeconfig = yamlencode({
    apiVersion      = "v1"
    kind            = "Config"
    current-context = module.eks.cluster_arn
    clusters = [{
      name = module.eks.cluster_arn
      cluster = {
        certificate-authority-data = module.eks.cluster_certificate_authority_data
        server                     = module.eks.cluster_endpoint
      }
    }]
    contexts = [{
      name = module.eks.cluster_arn
      context = {
        cluster = module.eks.cluster_arn
        user    = module.eks.cluster_arn
      }
    }]
    users = [{
      name = module.eks.cluster_arn
      user = {
        token = data.aws_eks_cluster_auth.this.token
      }
    }]
  })
}

resource "null_resource" "apply" {
  triggers = {
    kubeconfig = base64encode(local.kubeconfig)
    cmd_patch  = <<-EOT
      kubectl create configmap aws-auth -n kube-system --kubeconfig <(echo $KUBECONFIG | base64 -d)
      kubectl patch configmap/aws-auth --patch "${module.eks.aws_auth_configmap_yaml}" -n kube-system --kubeconfig <(echo $KUBECONFIG | base64 -d)
    EOT
  }

  provisioner "local-exec" {
    interpreter = ["/bin/bash", "-c"]
    environment = {
      KUBECONFIG = self.triggers.kubeconfig
    }
    command = self.triggers.cmd_patch
  }
}

resource "local_file" "kubeconfig" {
  content  = local.kubeconfig
  filename = "${path.module}/kubeconfig"
}


##################################################
# VPC Module
##################################################

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.13.0"

  name = "${local.name}-vpc"
  cidr = var.vpc.cidr

  azs                  = toset(data.aws_availability_zones.az_list.names)
  private_subnets      = var.private_subnets
  public_subnets       = var.public_subnets
  enable_nat_gateway   = var.vpc.enable_nat_gateway
  single_nat_gateway   = var.vpc.single_nat_gateway
  enable_dns_hostnames = var.vpc.enable_dns_hostnames
  #  enable_flow_log = true
  #  create_flow_log_cloudwatch_iam_role = true
  #  create_flow_log_cloudwatch_log_group = true
  public_subnet_tags = {
    "kubernetes.io/cluster/${local.name}-eks" = "shared"
    "kubernetes.io/role/elb"              = 1
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${local.name}-eks" = "shared"
    "kubernetes.io/role/elb"              = 1
  }

  tags = local.common_tags
}