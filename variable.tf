variable "vpc" {
  type = object({
    cidr                 = string
    enable_nat_gateway   = bool
    single_nat_gateway   = bool
    enable_dns_hostnames = bool
  })
}

variable "eks" {
  type = object({
    cluster_version = string
    spot = object({
      min_size      = number
      max_size      = number
      desired_size  = number
      instance_type = string
    })
  })
}

variable "region" {
  type = string
  default = "ap-southeast-1"
}

variable "private_subnets" {
  type = list(string)
}

variable "public_subnets" {
  type = list(string)
}

variable "environment" {
  type = string
}

variable "agent_namespace" {
  default     = "eks-agent"
  description = "Kubernetes namespace to install the Agent"
}

variable "agent_token" {
  description = "Agent token (provided after registering an Agent in GitLab)"
  type        = string
  sensitive   = true
}

variable "agent_version" {
  default     = "v14.8.1"
  description = "Agent version"
}

variable "kas_address" {
  description = "Agent Server address (provided after registering an Agent in GitLab)"
  default     = "wss://kas.gitlab.com"
}

variable "helm_repo" {
  default     = "https://kubernetes.github.io/ingress-nginx"
  description = "EKS Helm repo"
}