resource "helm_release" "ingress" {
  name             = local.name
  chart            = "ingress-nginx"
  repository       = var.helm_repo
  depends_on       = [module.eks]
  namespace        = "kube-system"
  create_namespace = false
  atomic           = true
  wait             = true
  wait_for_jobs    = true
  timeout          = 600

  set {
    name = "controller.admissionWebhooks.enabled"
    value = false
  }
}
